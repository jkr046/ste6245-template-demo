#ifndef GROUND_H
#define GROUND_H

#include <gmlib2/qt/exampleobjects/parametric/psurface.h>

// qt
#include <QQmlEngine>


namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

  class Ground : public gmlib2::qt::parametric::PSurface<
                   gmlib2::parametric::PPlane<gmlib2::qt::SceneObject>> {
    using Base = PSurface<gmlib2::parametric::PPlane<gmlib2::qt::SceneObject>>;
    Q_OBJECT

    Q_PROPERTY(
      gmlib2::qt::parametric::PSurfaceMesh* defaultMesh READ defaultMesh)

  public:

    // Constructor(s)
    template <typename... Ts>
    Ground(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    Q_INVOKABLE void setParametersQt(const QVector3D& p, const QVector3D& u,
                                     const QVector3D& v)
    {
      m_pt = VectorH_Type{double(p.x()), double(p.y()), double(p.z()), 1.0};
      m_u  = VectorH_Type{double(u.x()), double(u.y()), double(u.z()), 0.0};
      m_v  = VectorH_Type{double(v.x()), double(v.y()), double(v.z()), 0.0};
      if (defaultMesh()) defaultMesh()->reSample();
    }

    static void registerQmlTypes(int version_major, int version_minor)
    {
      if(qt_types_initialized) return;

      constexpr auto registertype_uri = "com.uit.STE6245";
//      qDebug() << "Trying to register type <Ground>; getting id: " <<
      qmlRegisterType<Ground>(registertype_uri, version_major, version_minor,
                              "Ground");

      qt_types_initialized = true;
    }

    // Signal(s)
  signals:

  private:
    static bool qt_types_initialized;
  };

}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace


#endif   // GROUND_H
