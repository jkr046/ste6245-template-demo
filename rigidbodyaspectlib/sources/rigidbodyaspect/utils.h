#ifndef RIGIDBODYASPECT_UTILS_H
#define RIGIDBODYASPECT_UTILS_H

#include "types.h"

namespace rigidbodyaspect::utils
{

  GM2Vector qVecToGM2Vec3(const QVector3D& qvec);

  QVector3D gm2VecToQVec3(const GM2Vector& bvec);

  std::tuple<GM2Vector, GM2Vector, GM2Vector>
  dupFrameToDUP(const QMatrix3x3& dup_frame);


}   // namespace rigidbodyaspect::utils






// qt system helpers
inline bool qFuzzyCompare(const QMatrix3x3& m1, const QMatrix3x3& m2)
{
  return qFuzzyCompare(m1(0, 0), m2(0, 0)) and qFuzzyCompare(m1(0, 1), m2(0, 1))
         and qFuzzyCompare(m1(0, 2), m2(0, 2))
         and qFuzzyCompare(m1(1, 0), m2(1, 0))
         and qFuzzyCompare(m1(1, 1), m2(1, 1))
         and qFuzzyCompare(m1(1, 2), m2(1, 2))
         and qFuzzyCompare(m1(2, 0), m2(2, 0))
         and qFuzzyCompare(m1(2, 1), m2(2, 1))
         and qFuzzyCompare(m1(2, 2), m2(2, 2));
}




#endif // RIGIDBODYASPECT_UTILS_H
