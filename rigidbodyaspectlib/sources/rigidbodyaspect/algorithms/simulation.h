#ifndef RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
#define RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H

#include "../geometry/movingbody.h"
#include "../backend/rigidbodyaspect.h"
#include "../jobs/genericsimjob.h"
#include "../backend/environmentbackend.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(MovingBody& body, seconds_type collisionTimePoint, seconds_type timeFrame);

}   // namespace rigidbodyaspect::algorithms::simulation


#endif   // RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
