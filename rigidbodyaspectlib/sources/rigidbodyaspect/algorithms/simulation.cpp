#include "simulation.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../jobs/genericsimjob.h"
#include "../backend/environmentbackend.h"

namespace rigidbodyaspect::algorithms::simulation
{
  void simulateGenericMovingBody(MovingBody& body,
                                 seconds_type local_dt, seconds_type global_dt)
  {

    // compute dynamics
      const auto ds = body.ds * (local_dt.count()/global_dt.count());

    // update physical properties
    body.translateLocal(ds);
  }

}   // namespace rigidbodyaspect::algorithms::dynamics
