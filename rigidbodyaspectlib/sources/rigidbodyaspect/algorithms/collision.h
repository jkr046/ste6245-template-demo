#ifndef RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
#define RIGIDBODYASPECT_ALGORITHMS_COLLISION_H

#include "../types.h"
#include "../geometry/rbtypes.h"
#include "../constants.h"

// stl
#include <variant>

namespace rigidbodyaspect {
class RigidBodyAspect;

}

namespace rigidbodyaspect::algorithms::collision
{

  namespace detail
  {

    enum class ColliderStatus { NoCollision , Collision};
    struct CollisionObjectSP {
        rbtypes::Sphere* sphere;
        rbtypes::FixedPlane* plane;
        seconds_type impactPoint;
    };

    struct CollisionObjectSS {
        rbtypes::Sphere* sphere1;
        rbtypes::Sphere* sphere2;
        seconds_type impactPoint;
    };

    struct CollisionObjectSC {

    };

    using ColliderReturnType = std::pair<seconds_type, ColliderStatus>;
  }   // namespace detail

  double computeCollision
  (const rbtypes::Sphere&sphere,const rbtypes::FixedPlane& plane, seconds_type dt);

  void computeCacheProperties
  (rbtypes::Sphere& sphere,RigidBodyAspect*  aspect,seconds_type dt);

  detail::ColliderReturnType 
  detectCollisionBetweenMovingSphereAndStaticPlane
  (const rbtypes::Sphere& sphere,const rbtypes::FixedPlane& plane, seconds_type dt);

  detail::ColliderReturnType
  detectCollisionBetweenTwoSpheres
  (rbtypes::Sphere& sphere1, rbtypes::Sphere& sphere2, seconds_type dt);


  void sortAndMakeUnique
  (std::vector<algorithms::collision::detail::CollisionObjectSP>& SPList,
   std::vector<algorithms::collision::detail::CollisionObjectSS>& SSList);


namespace collisionresponse
{

void CollisionBetweenMovingSphereAndStaticPlane
(rbtypes::Sphere& sphere, rbtypes::FixedPlane& plane, seconds_type dt);

void CollisionBetweenMovingSpheres
(rbtypes::Sphere& sphere1, rbtypes::Sphere& sphere2, seconds_type dt);

//void collisionBetweenMogvingSphereAndCyllinder
//(rbtypes::Sphere& sphere, rbtypes::)

}//namespace collisionresponse

namespace collisionvalue {
    using SpherePtr = std::unique_ptr<rbtypes::Sphere>;
    using FixedPlanePtr = std::unique_ptr<rbtypes::FixedPlane>;
    using SphereContainer = std::vector<SpherePtr>;
    using FixedPlaneContainer = std::vector<FixedPlanePtr>;
    using ColObjSP = algorithms::collision::detail::CollisionObjectSP;
    using ColObjSS = algorithms::collision::detail::CollisionObjectSS;

} //namespace collisionvalue

}   // namespace rigidbodyaspect::algorithms::collision


#endif   // RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
