#include "collision.h"
#include "../backend/rigidbodyaspect.h"
#include <set>
#include "../backend/environmentbackend.h"
#include "simulation.h"
#include "../constants.h"
#include "states.h"

namespace rigidbodyaspect::algorithms::collision
{

  void computeCacheProperties(rbtypes::Sphere &sphere, RigidBodyAspect *aspect, seconds_type dt)
  {
      const auto env_id = sphere.m_env_id;
      const auto env_BE = aspect->environmentBackend(env_id);
      const auto Gn     = env_BE ? env_BE->m_gravity : GM2Vector{0, 20.0, 0};
      const auto F      = Gn;


      // compute dynamics
      sphere.dt = dt;
      const auto a = F * dt.count();
      sphere.m_velocity += a;
      sphere.ds = (sphere.m_velocity + 0.5 * a) * dt.count();

  }


  detail::ColliderReturnType detectCollisionBetweenMovingSphereAndStaticPlane(
    const rbtypes::Sphere& sphere, const rbtypes::FixedPlane& plane, seconds_type dt)
  {

    const auto pl_eval
      = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                             rbtypes::FixedPlane::PSizeArray{1UL, 1UL});

    const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
    const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));

    const auto q = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));
    const auto n = blaze::evaluate(blaze::normalize(blaze::cross(pl_u, pl_v)));

    const auto p = sphere.frameOriginParent();
    const auto r = sphere.m_radius;
    const auto v = sphere.m_velocity;

    // d is the distance between the center of the sphere and plane
    const auto d = blaze::evaluate((q + r * n) - p);

    if(blaze::length(d) < constants::epsilon)
    return detail::ColliderReturnType{seconds_type(0), detail::ColliderStatus::NoCollision};

    // sphere's vector velocity
    const auto ds = sphere.ds;

    // trajectory of the sphere along the plane's normal
    const auto R = double(blaze::inner(ds, n));

    // distance between the sphere's shell and the plane
    const auto Q = double(blaze::inner(d, n));

    const auto x = Q / R;

    std::cout <<"x value for sphere & plane:" <<x<<std::endl;

    if (x > 0 && x <= 1)
      return detail::ColliderReturnType{x * dt, detail::ColliderStatus::Collision};
    return detail::ColliderReturnType{seconds_type(0), detail::ColliderStatus::NoCollision};
  };


  detail::ColliderReturnType
  detectCollisionBetweenTwoSpheres(rbtypes::Sphere& sphere1,
                                   rbtypes::Sphere& sphere2, seconds_type dt)
  {


    const auto ds1 = sphere1.m_velocity * dt.count();
    const auto ds2 = sphere2.m_velocity * dt.count();

    const auto r = sphere1.m_radius + sphere2.m_radius;

    const auto R = ds2 - ds1;
    const auto Q = sphere2.frameOriginParent() - sphere1.frameOriginParent();

    const auto a = blaze::inner(R, R);
    const auto b = blaze::inner(Q,R);
    const auto c = blaze::inner(Q, Q) - pow(r, 2);

    const auto x = (-b - sqrt(pow(b,2) - a*c)) / a;



// std::cout <<x<<std::endl;

    if (x > 0 && x <= 1)
    {

        return detail::ColliderReturnType
        {x * dt, detail::ColliderStatus::Collision};
    }

    return detail::ColliderReturnType{seconds_type(0),
                                      detail::ColliderStatus::NoCollision};
  };


  void sortAndMakeUnique(
    std::vector<algorithms::collision::detail::CollisionObjectSP>& SPList,
    std::vector<algorithms::collision::detail::CollisionObjectSS>& SSList )
  {

    using SphereSet = std::set<rbtypes::Sphere*>;
    using ColObjSP  = detail::CollisionObjectSP;
    using ColObjSS  = detail::CollisionObjectSS;

    // sorts the vector based on the time of collision (earliest first)
    std::sort(std::begin(SPList), std::end(SPList),
              [](const auto& a, const auto& b) { return a.impactPoint < b.impactPoint; });

    std::sort(std::begin(SSList), std::end(SSList),
              [](const auto& a, const auto& b) { return a.impactPoint < b.impactPoint; });

    SphereSet  unique_spheresSP;
     auto unique_endSP
      = std::remove_if(std::begin(SPList), std::end(SPList),
                       [&unique_spheresSP](const ColObjSP& col_obj) {
                         auto* sphere = col_obj.sphere;
                         if (unique_spheresSP.count(sphere)) return true;
                         unique_spheresSP.insert(sphere);
                         return false;
                       });

    SPList.erase(unique_endSP, std::end(SPList));

    SphereSet unique_spheresSS;
     auto unique_endSS
      = std::remove_if(std::begin(SSList), std::end(SSList),
                       [&unique_spheresSS](const ColObjSS& col_obj) {
                         auto* sphere1 = col_obj.sphere1;
                         auto* sphere2 = col_obj.sphere2;
                         if (unique_spheresSS.count(sphere1))
                         {
                            if (!unique_spheresSS.count(sphere2))
                                     unique_spheresSS.insert(sphere2);
                            return true;
                         }

                         if (unique_spheresSS.count(sphere2))
                         {
                             if(!unique_spheresSS.count(sphere1))
                                 unique_spheresSS.insert(sphere1);
                             return true;
                         }
                         unique_spheresSS.insert(sphere1);
                         unique_spheresSS.insert(sphere2);
                         return false;
                       });

    SSList.erase(unique_endSS, std::end(SSList));

    SphereSet uniqueSpheres;
    SphereSet nonUniqueSpheres;

    auto SPListIterator = SPList.cbegin();
    auto SSListIterator = SSList.begin();

    while(SPListIterator != SPList.end() && SSListIterator != SSList.end())
    {

        if(SSList.empty() || (SPListIterator->impactPoint < SSListIterator->impactPoint))
        {
            if(uniqueSpheres.count(SPListIterator->sphere))
                nonUniqueSpheres.insert(SPListIterator->sphere);
            else
                uniqueSpheres.insert(SPListIterator->sphere);
            SPListIterator ++;
        }

        else if(SPList.empty() || SSListIterator->impactPoint < SPListIterator->impactPoint)
        {
            if (uniqueSpheres.count(SSListIterator->sphere1))
            {
               nonUniqueSpheres.insert(SSListIterator->sphere1);
               if (!uniqueSpheres.count(SSListIterator->sphere2))
                        uniqueSpheres.insert(SSListIterator->sphere2);
            }

            if (uniqueSpheres.count(SSListIterator->sphere2))
            {
                nonUniqueSpheres.insert(SSListIterator->sphere2);
                if(!uniqueSpheres.count(SSListIterator->sphere1))
                    uniqueSpheres.insert(SSListIterator->sphere1);
            }
            uniqueSpheres.insert(SSListIterator->sphere1);
            uniqueSpheres.insert(SSListIterator->sphere2);
            SSListIterator ++;
        }
    }

     unique_endSP
      = std::remove_if(std::begin(SPList), std::end(SPList),
                       [&nonUniqueSpheres](const ColObjSP& col_obj) {
                         auto* sphere = col_obj.sphere;
                         if (nonUniqueSpheres.count(sphere)) return true;
                         return false;
                       });
     SPList.erase(unique_endSP, std::end(SPList));

     unique_endSS
      = std::remove_if(std::begin(SSList), std::end(SSList),
                       [&nonUniqueSpheres](const ColObjSS& col_obj) {
                         auto* sphere1 = col_obj.sphere1;
                         auto* sphere2 = col_obj.sphere2;
                         if (nonUniqueSpheres.count(sphere1))
                         {
                            if (!nonUniqueSpheres.count(sphere2))
                            return true;
                         }

                         if (nonUniqueSpheres.count(sphere2))
                         {
                             if(!nonUniqueSpheres.count(sphere1))
                             return true;
                         }
                         return false;
                       });

   SSList.erase(unique_endSS, std::end(SSList));

    }

  namespace collisionresponse {

  void CollisionBetweenMovingSphereAndStaticPlane
  (rbtypes::Sphere& sphere, rbtypes::FixedPlane& plane,[[maybe_unused]] seconds_type dt){

      const auto pl_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                                                rbtypes::FixedPlane::PSizeArray{1UL,1UL});

      const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(0UL,1UL));
      const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(1UL,0UL));
      const auto n = blaze::normalize(blaze::cross(pl_u,pl_v));
      const auto v = sphere.m_velocity;


      sphere.m_velocity = v - 2*blaze::inner(v,n)*n;
      std::cout<<"collision detected and changing velocity"<<std::endl;
  }


  void CollisionBetweenMovingSpheres
  (rbtypes::Sphere& sphere1, rbtypes::Sphere& sphere2,[[maybe_unused]] seconds_type dt){

      const auto numerator = sphere2.frameOriginParent()-sphere1.frameOriginParent();
      const auto denominator = blaze::length((sphere2.frameOriginParent()-sphere1.frameOriginParent()));


      const auto d = (numerator) / (denominator);

//      std::cout<<d<<std::endl;

      const gmlib2::DVectorT <double> temp {d[0],d[1],d[2]};
      const auto dlin = gmlib2::algorithms::linearIndependentVector(temp);
      const auto dlin_d = blaze::cross(dlin,d);
      const auto n = dlin_d/blaze::length(dlin_d);

      const auto v1d = blaze::inner(sphere1.m_velocity,d);
      const auto v2d = blaze::inner(sphere2.m_velocity,d);
      const auto v1n = blaze::inner(sphere1.m_velocity,n);
      const auto v2n = blaze::inner(sphere2.m_velocity,n);
      const auto m1 = sphere1.m_mass;
      const auto m2 = sphere2.m_mass;

      const auto v1dird = ((m1 - m2)/(m1 + m2))*v1d +((2*m2)/(m1+m2))*v2d;
      const auto v2dird = ((m2 - m1)/(m1 + m2))*v2d +((2*m1)/(m1+m2))*v1d;

      const auto v1dir = v1n*n + v1dird*d;
      const auto v2dir = v2n*n + v2dird*d;

      sphere1.m_velocity = v1dir;
      sphere2.m_velocity = v2dir;
  }


  }


}   // namespace rigidbodyaspect::algorithms::collision
