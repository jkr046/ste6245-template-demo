#include "states.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"
#include "../constants.h"
#include "../geometry/movingbody.h"

namespace rigidbodyaspect::algorithms::states {

    MovingBody::State detectStateChange(rbtypes::Sphere &sphere, rbtypes::FixedPlane &plane, seconds_type dt){

//        if (sphere.curState != MovingBody::State::free)
//            return sphere.curState;
    const auto pl_eval
      = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                             rbtypes::FixedPlane::PSizeArray{1UL, 1UL});

    const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
    const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));

    const auto q = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));
    const auto n = blaze::evaluate(blaze::normalize(blaze::cross(pl_u, pl_v)));

    const auto p = sphere.frameOriginParent();
    const auto r = sphere.m_radius;
    const auto v = sphere.m_velocity;


    // d is the distance between the center of the sphere and plane
    const auto d = blaze::evaluate((q + r * n) - p);


    // sphere's vector velocity
    const auto ds = sphere.ds;

    // trajectory of the sphere along the plane's normal
    const auto R = double(blaze::inner(ds, n));

    // distance between the sphere's shell and the plane
    const auto Q = double(blaze::inner(d, n));

    //ds linearly dependet between the sphere and plane
    const auto linDep = std::abs(blaze::inner(-n*r,ds) - blaze::inner(ds,ds));

    const auto x = Q / R;
    

    //is at rest
    if(linDep < constants::epsilon)
    {
     return MovingBody::State::atRest;
    }

    //is free
    if (R > 0)
    {        
        return MovingBody::State::free;
    }

    //is sliding
    if (R <= 0)
    {
        return MovingBody::State::sliding;
    }

}

    void detectStateChange(rbtypes::Sphere &sphere1, rbtypes::Sphere sphere2)
    {

    }

    void atRest(rbtypes::Sphere &sphere, rbtypes::FixedPlane &plane){

    }

    void sliding (rbtypes::Sphere &sphere, rbtypes::FixedPlane& plane){

        const auto pl_eval
          = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                                 rbtypes::FixedPlane::PSizeArray{1UL, 1UL});

        const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
        const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));

        const auto q = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));
        const auto n = blaze::normalize(blaze::cross(pl_u, pl_v));

        const auto p = sphere.frameOriginParent();
        const auto r = sphere.m_radius;
        const auto v = sphere.m_velocity;

        const auto ds = sphere.ds;

        //sphere's next postition (past/through the plane)
        const auto np = p + ds;

        //correction vector (brings the sphere back to the surface)
        const auto d = (q - np) + n * r;

        //new trajectory vector
        sphere.ds = ds + d;

    }

    void free (rbtypes::Sphere &sphere, rbtypes::FixedPlane& plane)
    {

    }




}
