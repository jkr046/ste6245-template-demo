#ifndef RIGIDBODYASPECT_ALGORITHMS_STATES_H
#define RIGIDBODYASPECT_ALGORITHMS_STATES_H

#include "../types.h"
#include "../geometry/rbtypes.h"


namespace rigidbodyaspect {

class rigidbodyaspect;

}

namespace rigidbodyaspect::algorithms::states {

//    enum class StateChange {free, atRest, sliding};

    struct StateChangeEventSP {
        rbtypes::Sphere* sphere;
        rbtypes::FixedPlane* plane;
        seconds_type impactPoint;
        MovingBody::State changeTo;
    };
    
    struct StateChangeEventSS {
        rbtypes::Sphere* sphere1;
        rbtypes::Sphere* sphere2;
        seconds_type impactPoint;
    };

    struct StateChangeEventSC {

    };
    
    using StateReturnType = std::pair<seconds_type, MovingBody::State>;

    MovingBody::State detectStateChange
    (rbtypes::Sphere &sphere, rbtypes::FixedPlane& plane, seconds_type dt);

    void detectStateChange
    (rbtypes::Sphere &sphere1, rbtypes::Sphere sphere2);

    void atRest (rbtypes::Sphere &sphere, rbtypes::FixedPlane& plane);

    void sliding (rbtypes::Sphere &sphere, rbtypes::FixedPlane& plane);

    void free (rbtypes::Sphere &sphere, rbtypes::FixedPlane& plane);
}




#endif // STATES_H
