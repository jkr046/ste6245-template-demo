#include "genericcolliderjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/collision.h"
#include "../algorithms/simulation.h"
#include "../algorithms/states.h"
#include <iomanip>
#include <chrono>
// qt
#include <QDebug>

namespace rigidbodyaspect
{
using namespace algorithms::collision;

  GenericColliderJob::GenericColliderJob(RigidBodyAspect* aspect)
    : m_aspect{aspect}
  {
  }

  void GenericColliderJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void GenericColliderJob::run()
  {
    auto& rigid_bodies = m_aspect->rigidBodies();

    std::vector<collisionvalue::ColObjSP> CSP; //collision between sphere & plane
    std::vector<collisionvalue::ColObjSS> CSS; //collision between sphere & sphere
    std::vector<algorithms::states::StateChangeEventSP> SSP; //store sphere that will have a state change
    
    for(auto& sphere : rigid_bodies.spheres())
    {
        if(sphere.curState != MovingBody::State::atRest)
        computeCacheProperties(sphere, m_aspect, m_dt);
        sphere.tp = seconds_type(0);
    }

    for (auto& sphere : rigid_bodies.spheres()) {
      for (auto& fixed_plane : rigid_bodies.fixedPlanes()) {
       const auto returnValue = detectCollisionBetweenMovingSphereAndStaticPlane(sphere, fixed_plane,
                                                      m_dt);              
       if(returnValue.second == detail::ColliderStatus::Collision)
       {
          detail::CollisionObjectSP c {&sphere, &fixed_plane, returnValue.first};
          CSP.push_back(c);
       }

     }
   }

    for (auto& sphere1 : rigid_bodies.spheres()) {
      for (auto& sphere2 : rigid_bodies.spheres()) {
       const auto returnValue = detectCollisionBetweenTwoSpheres(sphere1, sphere2,
                                                      m_dt);
       if(returnValue.second == detail::ColliderStatus::Collision)
       {
          detail::CollisionObjectSS c {&sphere1, &sphere2, returnValue.first};
          CSS.push_back(c);
       }
     }
   }

   sortAndMakeUnique(CSP, CSS);

   while (!CSP.empty() || !CSS.empty()) {

    bool a = false;
    bool b = false;

    if(CSS.empty() && !CSP.empty())
        a = true;
    else if (!CSS.empty() && !CSP.empty())
        if(CSP.front().impactPoint < CSS.front().impactPoint)
     a = true;

    if (CSP.empty() && !CSS.empty())
        b = true;
    else if (!CSP.empty() && !CSS.empty())
        if(CSS.front().impactPoint < CSP.front().impactPoint)
        b = true;

     if (a) {

       const auto curval = CSP.front();

       const auto sphere      = curval.sphere;
       const auto impactPoint = curval.impactPoint;
       const auto plane       = curval.plane;

       CSP.erase(CSP.begin());

       //simulate all moving objects to the point of first impact unless it's "at rest"
       if(sphere->curState != MovingBody::State::atRest)
       for(auto& a : rigid_bodies.spheres())
       algorithms::simulation::simulateGenericMovingBody(a, impactPoint,
                                                         m_dt);
       sphere->tp = impactPoint;

       std::cout <<"****************************************************"
                <<'\n' << " time of collionsion: " << sphere->tp.count() << '\n' <<
                    " remaining time: " << sphere->dt.count()<< '\n' <<
                    " sphere pos: " << '\n' << sphere->frameOriginParent() << '\n'
               << "*****************************************************" << '\n';

       algorithms::collision::collisionresponse::
         CollisionBetweenMovingSphereAndStaticPlane(*sphere, *plane, impactPoint);

       for (auto& a : rigid_bodies.spheres())
         computeCacheProperties(a, m_aspect, m_dt);

       const auto STC = algorithms::states::detectStateChange(*sphere, *plane, impactPoint);
       if(sphere->curState != STC)
           sphere->curState = STC;

       if(sphere->curState == MovingBody::State::free)
       std::cout << '\n' <<"state: free "<< '\n' ;
       else if(sphere->curState == MovingBody::State::sliding)
           std::cout << '\n' << "state: sliding "<< '\n' ;
       else if(sphere->curState == MovingBody::State::atRest)
           std::cout << '\n' << "state: at rest "<< '\n' ;

       for (auto& sphere : rigid_bodies.spheres()) {
         for (auto& fixed_plane : rigid_bodies.fixedPlanes()) {
           const auto returnValue
             = detectCollisionBetweenMovingSphereAndStaticPlane(
               sphere, fixed_plane, impactPoint);
           if (returnValue.second == detail::ColliderStatus::Collision) {
             detail::CollisionObjectSP c{&sphere, &fixed_plane,
                                         returnValue.first};
             CSP.push_back(c);         

           }
         }
       }

       for (auto& sphere1 : rigid_bodies.spheres()) {
         for (auto& sphere2 : rigid_bodies.spheres()) {
           const auto returnValue
             = detectCollisionBetweenTwoSpheres(sphere1, sphere2, m_dt - impactPoint);
           if (returnValue.second == detail::ColliderStatus::Collision) {
             detail::CollisionObjectSS c{&sphere1, &sphere2, returnValue.first};
             CSS.push_back(c);
           }
         }
       }



     }


     else if (b) {

       const auto curval = CSS.front();

       const auto sphere1     = curval.sphere1;
       const auto sphere2     = curval.sphere2;
       const auto impactPoint = curval.impactPoint;

       CSS.erase(CSS.begin());

       algorithms::simulation::simulateGenericMovingBody(*sphere1, impactPoint,
                                                         m_dt);
       algorithms::simulation::simulateGenericMovingBody(*sphere2, impactPoint,
                                                         m_dt);

       algorithms::collision::collisionresponse::CollisionBetweenMovingSpheres(
         *sphere1, *sphere2, impactPoint);
       sphere1->tp = impactPoint;
       sphere2->tp = impactPoint;

       for (auto a : rigid_bodies.spheres())
         computeCacheProperties(a, m_aspect, sphere1->tp);

       for (auto& sphere : rigid_bodies.spheres()) {
         for (auto& fixed_plane : rigid_bodies.fixedPlanes()) {
           const auto returnValue
             = detectCollisionBetweenMovingSphereAndStaticPlane(
               sphere, fixed_plane, m_dt - impactPoint);
           if (returnValue.second == detail::ColliderStatus::Collision) {
             detail::CollisionObjectSP c{&sphere, &fixed_plane,
                                         returnValue.first};
             CSP.push_back(c);
           }

           for (auto& sphere1 : rigid_bodies.spheres()) {
             for (auto& sphere2 : rigid_bodies.spheres()) {
               const auto returnValue = detectCollisionBetweenTwoSpheres(
                 sphere1, sphere2, m_dt - impactPoint);
               if (returnValue.second == detail::ColliderStatus::Collision) {
                 detail::CollisionObjectSS c{&sphere1, &sphere2,
                                             returnValue.first};
                 CSS.push_back(c);
               }
             }
           }
         }
       }

     }

     sortAndMakeUnique(CSP, CSS);
   }


   for(auto& sphere : rigid_bodies.spheres())
   {
              if(sphere.curState != MovingBody::State::atRest)
             algorithms::simulation::simulateGenericMovingBody(sphere, sphere.dt , m_dt);
   }

  }



}   // namespace rigidbodyaspect
