#include "genericsimjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/simulation.h"

// qt
#include <QDebug>

namespace rigidbodyaspect
{

  GenericSimJob::GenericSimJob(RigidBodyAspect* aspect) : m_aspect{aspect} {}

  void GenericSimJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void GenericSimJob::run()
  {
    auto& rigid_bodies = m_aspect->rigidBodies();

    for (auto& sphere : rigid_bodies.spheres()) {

      const auto env_id = sphere.m_env_id;
      const auto env_BE = m_aspect->environmentBackend(env_id);
      const auto Gn     = env_BE ? env_BE->m_gravity : GM2Vector{0, 20.0, 0};
      const auto F      = Gn;

//      algorithms::simulation::simulateGenericMovingBody(sphere, sphere.dt , m_dt);
    }
  }

}   // namespace rigidbodyaspect
