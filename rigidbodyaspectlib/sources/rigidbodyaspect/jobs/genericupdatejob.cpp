#include "genericupdatejob.h"
#include "../backend/spherecontrollerbackend.h"
#include "../backend/rigidbodyaspect.h"

// qt
#include <QDebug>

namespace rigidbodyaspect
{

  GenericUpdateJob::GenericUpdateJob(RigidBodyAspect* aspect) : m_aspect{aspect}
  {
  }

  void GenericUpdateJob::run()
  {
    const auto& rigid_bodies = m_aspect->rigidBodies();
    const auto& spheres      = rigid_bodies.spheres();

    for( const auto& id : spheres.keys()) {

      const auto& backend = m_aspect->sphereControllerBackend(id);
      backend->queueFrontendUpdate();
    }
  }

}   // namespace rigidbodyaspect
