#ifndef RIGIDBODYASPECT_SPHERECONTROLLER_H
#define RIGIDBODYASPECT_SPHERECONTROLLER_H


#include "abstractrigidbodycontroller.h"
#include <QVector3D>

namespace rigidbodyaspect
{

  class SphereController : public AbstractRigidBodyController {
    Q_OBJECT

    Q_PROPERTY(float radius MEMBER m_radius NOTIFY radiusChanged)

      Q_PROPERTY(QVector3D velocity MEMBER m_velocity NOTIFY velocityChanged)

  public:
    SphereController(Qt3DCore::QNode* parent = nullptr);


    float m_radius{1.0f};
    QVector3D m_velocity{0,0,0};

  signals:
    void radiusChanged(float radius);

    void velocityChanged(QVector3D velocity);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr createNodeCreationChange() const override;
  };


  struct SphereInitialData : AbstractRigidBodyInitialData {
    float m_radius;
    QVector3D m_velocity;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_SPHERECONTROLLER_H
