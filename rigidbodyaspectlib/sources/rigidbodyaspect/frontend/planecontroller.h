#ifndef RIGIDBODYASPECT_PLANECONTROLLER_H
#define RIGIDBODYASPECT_PLANECONTROLLER_H


#include "abstractrigidbodycontroller.h"

namespace rigidbodyaspect
{

  class PlaneController : public AbstractRigidBodyController {
    Q_OBJECT

    Q_PROPERTY(QVector3D p MEMBER m_p NOTIFY planePointChanged)
    Q_PROPERTY(QVector3D u MEMBER m_u NOTIFY planeUAxisChanged)
    Q_PROPERTY(QVector3D v MEMBER m_v NOTIFY planeVAxisChanged)

  public:
    PlaneController(Qt3DCore::QNode* parent = nullptr);


    QVector3D m_p{0, 0, 0};
    QVector3D m_u{10.0f, 0.0f, 0.0f};
    QVector3D m_v{0.0f, 0.0f, 10.0f};

  signals:
    void planePointChanged(const QVector3D& p);
    void planeUAxisChanged(const QVector3D& u);
    void planeVAxisChanged(const QVector3D& v);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr createNodeCreationChange() const override;
  };


  struct PlaneInitialData : AbstractRigidBodyInitialData {
    QVector3D m_p;
    QVector3D m_u;
    QVector3D m_v;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_PLANECONTROLLER_H
