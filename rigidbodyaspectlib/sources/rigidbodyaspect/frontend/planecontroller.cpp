#include "planecontroller.h"

namespace rigidbodyaspect
{




  PlaneController::PlaneController(Qt3DCore::QNode* parent)
    : AbstractRigidBodyController(parent)
  {
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  PlaneController::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<PlaneInitialData>::create(this);

    auto& data          = creationChange->data;
    data.m_dynamics_type  = m_dynamics_type;
    data.m_dup_frame      = m_dup_frame;
    data.m_environment_id = Qt3DCore::qIdForNode(m_environment);

    data.m_p = m_p;
    data.m_u = m_u;
    data.m_v = m_v;

    return std::move(creationChange);
  }

}   // namespace rigidbodyaspect
