#include "abstractrigidbodycontroller.h"


// gmlib2
#include <gmlib2/qt/scenegraph.h>
using namespace gmlib2::qt;

namespace rigidbodyaspect
{

  AbstractRigidBodyController::AbstractRigidBodyController(
    Qt3DCore::QNode* parent)
    : Qt3DCore::QComponent(parent)
  {
  }

  void AbstractRigidBodyController::sceneChangeEvent(
    const Qt3DCore::QSceneChangePtr& change)
  {
    if (change->type() == Qt3DCore::PropertyUpdated) {
      if (const auto e
          = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(change);
          e->propertyName() == QByteArrayLiteral("dupframe_BE")) {

        const auto dup = e->value().value<QMatrix3x3>();

        const auto was_blocked = blockNotifications(true);
        emit       frameComputed({dup(0, 0), dup(1, 0), dup(2, 0)},
                           {dup(0, 1), dup(1, 1), dup(2, 1)},
                           {dup(0, 2), dup(1, 2), dup(2, 2)});
        blockNotifications(was_blocked);
        return;
      }
    }

    QComponent::sceneChangeEvent(change);
  }

  QVariant AbstractRigidBodyController::dynamicsType() const
  {
    QVariant s;
    s.setValue(m_dynamics_type);
    return s;
  }

  void
  AbstractRigidBodyController::setDynamicsType(const QVariant& dynamics_type)
  {
    m_dynamics_type = dynamics_type.value<RigidBodyDynamicsType>();
  }

  QMatrix3x3 AbstractRigidBodyController::dupFrame() const
  {
    return m_dup_frame;
  }

  void AbstractRigidBodyController::resetFrameByDup(const QVector3D& dir,
                                                    const QVector3D& up,
                                                    const QVector3D& pos)
  {
    m_dup_frame(0,0) = dir.x();
    m_dup_frame(1,0) = dir.y();
    m_dup_frame(2,0) = dir.z();

    m_dup_frame(0,1) = up.x();
    m_dup_frame(1,1) = up.y();
    m_dup_frame(2,1) = up.z();

    m_dup_frame(0,2) = pos.x();
    m_dup_frame(1,2) = pos.y();
    m_dup_frame(2,2) = pos.z();

    dupFrameChanged(m_dup_frame);
  }

  Environment*AbstractRigidBodyController::environment() const
  {
    return m_environment;
  }

  void AbstractRigidBodyController::setEnvironment(Environment*  environment)
  {
    m_environment = environment;
    emit environmentChanged(m_environment);
  }

}   // namespace rigidbodyaspect
