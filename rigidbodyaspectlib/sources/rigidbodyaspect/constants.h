#ifndef RIGIDBODYASPECT_CONSTANTS_H
#define RIGIDBODYASPECT_CONSTANTS_H

#include "types.h"
#include "chrono"

namespace rigidbodyaspect::constants {

  constexpr auto Gn = GM2UnitType{980.665};  // [cm/s^2]
  const auto epsilon = 1e-3;
  seconds_type previousTime ();
  //seconds_type ;

}   // namespace rigidbodyaspect



#endif // RIGIDBODYASPECT_CONSTANTS_H
