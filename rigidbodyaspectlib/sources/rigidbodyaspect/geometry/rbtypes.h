#ifndef RIGIDBODYASPECT_RBTYPES_H
#define RIGIDBODYASPECT_RBTYPES_H

#include "movingbody.h"
#include "fixedbody.h"
namespace rigidbodyaspect::rbtypes {

    using Sphere     = gmlib2::parametric::PSphere<MovingBody>;
    using FixedPlane = gmlib2::parametric::PPlane<FixedBody>;
//    using Cylinder = gmlib2::parametric::
}



#endif // RIGIDBODYASPECT_RBTYPES_H
