#ifndef RIGIDBODYASPECT_MOVINGBODY_H
#define RIGIDBODYASPECT_MOVINGBODY_H

#include "../types.h"
namespace rigidbodyaspect
{

  class MovingBody : public GM2SpaceObjectType  {
  public:

    // members
    Unit_Type         m_mass;
    GM2Vector         m_velocity;
    Qt3DCore::QNodeId m_env_id;
    enum class State {free, atRest, sliding};
    //where the object is currently in time (time point)
    seconds_type      tp;
    //trajectory vector
    GM2Vector         ds;
    //delta time - tp
    //where the object is in relation to the current time frame (delta time) (NOT total timeframe)
    seconds_type      dt;


    //current state of object
    mutable State curState = State::free;

  };

}   // namespace rigidbodyaspect

#endif // RIGIDBODYASPECT_MOVINGBODY_H

