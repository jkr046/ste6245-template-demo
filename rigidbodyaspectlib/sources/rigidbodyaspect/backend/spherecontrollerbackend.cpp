#include "spherecontrollerbackend.h"

#include "rigidbodyaspect.h"
#include "../frontend/spherecontroller.h"
#include "../utils.h"
#include "../geometry/rbtypes.h"
#include <QVector3D>

namespace rigidbodyaspect
{

  SphereControllerBackend::SphereControllerBackend(RigidBodyContainer& rigid_bodies)
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite),
      m_rigid_bodies{rigid_bodies}
  {
  }

  SphereControllerBackend::~SphereControllerBackend()
  {
    m_rigid_bodies.destroySphere(peerId());
  }

  void SphereControllerBackend::queueFrontendUpdate()
  {
    auto aFrame = m_rigid_bodies.sphere(peerId()).aSpaceFrameParent();

    // Convert data to sendable frame
    QMatrix3x3 q_dupframe;
    for (auto i = 0UL; i < 3UL; ++i) {
      q_dupframe(int(i), 0) = float(aFrame(i, 0UL));   // dir
      q_dupframe(int(i), 1) = float(aFrame(i, 2UL));   // up
      q_dupframe(int(i), 2) = float(aFrame(i, 3UL));   // origin
    }

    // Send data
    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("dupframe_BE"));
    e->setValue(QVariant::fromValue(q_dupframe));
    notifyObservers(e);
  }

  void SphereControllerBackend::setRadius(float radius)
  {
    if (m_radius == radius) return;

    m_radius = radius;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    m_rigid_bodies.sphere(peerId()).m_radius = double(radius);
  }

  void SphereControllerBackend::setVelocity(QVector3D velocity)
  {
    if (m_velocity == velocity) return;

    m_velocity = velocity;

  }

  void SphereControllerBackend::setDupFrame(const QMatrix3x3 dup_frame)
  {
    if (qFuzzyCompare(dup_frame, m_dup_frame)) return;

    m_dup_frame = dup_frame;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    m_rigid_bodies.sphere(peerId()).setFrameParent(dir, up, pos);
  }

  void SphereControllerBackend::setDynamicsType(
    const RigidBodyDynamicsType& dynamics_type)
  {
    if (m_dynamics_type == dynamics_type) return;
  }

  void SphereControllerBackend::setEnvironmentId(const Qt3DCore::QNodeId& id)
  {
    if (m_environment_id == id) return;

    m_environment_id = id;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    m_rigid_bodies.sphere(peerId()).m_env_id = m_environment_id;
  }

  void SphereControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange
      = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<SphereInitialData>>(
        change);
    const auto& data = typedChange->data;
    m_dynamics_type  = data.m_dynamics_type;
    m_dup_frame      = data.m_dup_frame;
    m_radius         = data.m_radius;
    m_velocity       = data.m_velocity;
    m_environment_id = data.m_environment_id;

    m_rigid_bodies.constructSphere(peerId());
    auto& sphere = m_rigid_bodies.sphere(peerId());

    // geometric properties
    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    sphere.setFrameParent(dir, up, pos);
    sphere.m_radius = double(m_radius);
    sphere.m_env_id = m_environment_id;

    // init physical properties
    sphere.m_mass = 1.0;
    sphere.m_velocity = utils::qVecToGM2Vec3(m_velocity);
  }

  void
  SphereControllerBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {
    if (e->type() == Qt3DCore::PropertyUpdated) {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      if (change->propertyName() == QByteArrayLiteral("dupFrame"))
        setDupFrame(change->value().value<QMatrix3x3>());
      else if (change->propertyName() == QByteArrayLiteral("dynamicsType"))
        setDynamicsType(change->value().value<RigidBodyDynamicsType>());
      else if (change->propertyName() == QByteArrayLiteral("environment"))
        setEnvironmentId(change->value().value<Qt3DCore::QNodeId>());
      else if (change->propertyName() == QByteArrayLiteral("radius"))
        setRadius(change->value().toFloat());
      else if (change->propertyName() == QByteArrayLiteral("velocity"))
          setVelocity(change->value().value<QVector3D>());
    }

    QBackendNode::sceneChangeEvent(e);
  }

  SphereControllerBackendMapper::SphereControllerBackendMapper(
    RigidBodyAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* SphereControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new SphereControllerBackend(m_aspect->rigidBodies());
    m_aspect->addSphereControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  SphereControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->sphereControllerBackend(id);
  }

  void SphereControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takeSphereControllerBackend(id);
  }

}   // namespace rigidbodyaspect
