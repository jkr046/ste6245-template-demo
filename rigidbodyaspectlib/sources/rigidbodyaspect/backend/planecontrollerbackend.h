#ifndef RIGIDBODYASPECT_PLANECONTROLLERBACKEND_H
#define RIGIDBODYASPECT_PLANECONTROLLERBACKEND_H

#include "../types.h"

#include "../geometry/rigidbodycontainer.h"

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QBackendNode>

namespace rigidbodyaspect
{

  class PlaneControllerBackend : public Qt3DCore::QBackendNode {
  public:
    PlaneControllerBackend(RigidBodyContainer& rigid_bodies);
    ~PlaneControllerBackend() override;

    void queueFrontendUpdate();

  private:
    RigidBodyDynamicsType m_dynamics_type;
    QMatrix3x3            m_dup_frame;
    RigidBodyContainer&   m_rigid_bodies;
    Qt3DCore::QNodeId     m_environment_id;


    void setDupFrame(const QMatrix3x3 dup_frame);

    void setDynamicsType(const RigidBodyDynamicsType& dynamics_type);
    void setEnvironmentId(const Qt3DCore::QNodeId& id);


    rbtypes::FixedPlane& fixedPlane();

    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
  };


  class RigidBodyAspect;

  class PlaneControllerBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit PlaneControllerBackendMapper(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_PLANECONTROLLERBACKEND_H
