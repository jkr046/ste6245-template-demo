#include "planecontrollerbackend.h"

#include "rigidbodyaspect.h"
#include "../frontend/planecontroller.h"
#include "../utils.h"

namespace rigidbodyaspect
{

  PlaneControllerBackend::PlaneControllerBackend(RigidBodyContainer& rigid_bodies)
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite),
      m_rigid_bodies{rigid_bodies}
  {
  }

  PlaneControllerBackend::~PlaneControllerBackend()
  {
    m_rigid_bodies.destroyFixedPlane(peerId());
  }

  void PlaneControllerBackend::queueFrontendUpdate()
  {

    const auto& hframe = m_rigid_bodies.fixedPlane(peerId()).pSpaceFrameParent();

    // Convert data to sendable frame
    QMatrix3x3 q_hframe;
    for (auto i = 0UL; i < 3UL; ++i) {
      q_hframe(int(i), 0)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 0UL));   // dir
      q_hframe(int(i), 1)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 2UL));   // up
      q_hframe(int(i), 2)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 3UL));   // pos
    }

    // Send data
    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("dupframe_BE"));
    e->setValue(QVariant::fromValue(q_hframe));
    notifyObservers(e);
  }

  void PlaneControllerBackend::setDupFrame(const QMatrix3x3 dup_frame)
  {
    if (qFuzzyCompare(dup_frame, m_dup_frame)) return;

    m_dup_frame = dup_frame;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    m_rigid_bodies.fixedPlane(peerId()).setFrameParent(dir, up, pos);
  }

  void PlaneControllerBackend::setDynamicsType(
    const RigidBodyDynamicsType& dynamics_type)
  {
    if (m_dynamics_type == dynamics_type) return;
  }

  void PlaneControllerBackend::setEnvironmentId(const Qt3DCore::QNodeId& id)
  {
    if (m_environment_id == id) return;

    m_environment_id = id;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    m_rigid_bodies.fixedPlane(peerId()).m_env_id = m_environment_id;
  }

  void PlaneControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange
      = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<PlaneInitialData>>(
        change);
    const auto& data = typedChange->data;
    m_dynamics_type  = data.m_dynamics_type;
    m_dup_frame      = data.m_dup_frame;
    m_environment_id = data.m_environment_id;

    m_rigid_bodies.constructFixedPlane(peerId());
    auto& plane = m_rigid_bodies.fixedPlane(peerId());

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    plane.setFrameParent(dir, up, pos);
    plane.m_env_id = m_environment_id;

    plane.m_pt = GM2HVector{0,0,0};
    plane.m_u = GM2HVector{1,0,0};
    plane.m_v = GM2HVector{0,0,1};
  }

  void
  PlaneControllerBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {
    if (e->type() == Qt3DCore::PropertyUpdated) {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      if (change->propertyName() == QByteArrayLiteral("dupFrame"))
        setDupFrame(change->value().value<QMatrix3x3>());
      else if (change->propertyName() == QByteArrayLiteral("dynamicsType"))
        setDynamicsType(change->value().value<RigidBodyDynamicsType>());
      else if (change->propertyName() == QByteArrayLiteral("environment"))
        setEnvironmentId(change->value().value<Qt3DCore::QNodeId>());
    }

    QBackendNode::sceneChangeEvent(e);
  }

  PlaneControllerBackendMapper::PlaneControllerBackendMapper(
    RigidBodyAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* PlaneControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new PlaneControllerBackend(m_aspect->rigidBodies());
    m_aspect->addPlaneControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  PlaneControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->planeControllerBackend(id);
  }

  void PlaneControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takePlaneControllerBackend(id);
  }

}   // namespace rigidbodyaspect
