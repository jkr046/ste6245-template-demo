#include "simulatorsettingsbackend.h"

#include "../frontend/simulatorsettings.h"
#include "rigidbodyaspect.h"

namespace rigidbodyaspect
{

  SimulatorSettingsBackend::SimulatorSettingsBackend()
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite)
  {
  }

  bool SimulatorSettingsBackend::runStatus() const
  {
    return m_run_status;
  }

  void SimulatorSettingsBackend::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange
      = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<SimulatorSettingsInitialData>>(
        change);
    const auto& data = typedChange->data;
    m_run_status = data.m_run_status;
  }


  void SimulatorSettingsBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {
    if (e->type() == Qt3DCore::PropertyUpdated)
    {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      if (change->propertyName() == QByteArrayLiteral("runStatus")) {
        const auto new_value = change->value().toBool();
        if (m_run_status not_eq new_value) {
          m_run_status = new_value;
        }
      }
    }

    QBackendNode::sceneChangeEvent(e);
  }

  SimulatorSettingsBackendMapper::SimulatorSettingsBackendMapper(
    RigidBodyAspect* aspect)
    : m_aspect{aspect}
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* SimulatorSettingsBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    return m_aspect->constructSimulatorSettingsBackend(change->subjectId());
  }

  Qt3DCore::QBackendNode*SimulatorSettingsBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->simulatorSettingsBackend(id);
  }

  void SimulatorSettingsBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    m_aspect->releaseSimulatorSettingsBackend(id);
  }


}   // namespace rigidbodyaspect
