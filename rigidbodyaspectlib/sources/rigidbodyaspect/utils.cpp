#include "utils.h"

namespace rigidbodyaspect::utils {

  QVector3D gm2VecToQVec3(const GM2Vector& bvec)
  {
    return {float(bvec[0]),float(bvec[1]),float(bvec[2])};
  }

  GM2Vector qVecToGM2Vec3(const QVector3D& qvec)
  {
    return GM2Vector{double(qvec[0]), double(qvec[1]), double(qvec[2])};
  }

  std::tuple<GM2Vector, GM2Vector, GM2Vector>
  dupFrameToDUP(const QMatrix3x3& df)
  {

    return {qVecToGM2Vec3({df(0, 0), df(1, 0), df(2, 0)}),
            qVecToGM2Vec3({df(0, 1), df(1, 1), df(2, 1)}),
            qVecToGM2Vec3({df(0, 2), df(1, 2), df(2, 2)})};
  }


}   // namespace rigidbodyaspect::utils
