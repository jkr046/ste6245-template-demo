#ifndef RIGIDBODYASPECT_TYPES_H
#define RIGIDBODYASPECT_TYPES_H


// gmlib2
#include <gmlib2/qt/sceneobject.h>

// qt
#include <QGenericMatrix>

// stl
#include <chrono>



namespace rigidbodyaspect
{

  // std::ratio<1,1> is implicit
  // default and means seconds
  //
  // Literals are enabled through:
  // using namespace std::chrono_literals;
  using seconds_type = std::chrono::duration<double>;

  using GM2SpaceObjectType = gmlib2::ProjectiveSpaceObject<>;
  using GM2UnitType        = GM2SpaceObjectType::Unit_Type;
  using GM2Vector          = GM2SpaceObjectType::Vector_Type;
  using GM2HVector         = GM2SpaceObjectType::VectorH_Type;
  using GM2HFrame          = GM2SpaceObjectType::ASFrameH_Type;


}   // namespace rigidbodyaspect



// QML asseccible types
namespace rigidbodyaspect
{
  Q_NAMESPACE
  enum class RigidBodyDynamicsType { StaticObject, DynamicObject };
  Q_ENUM_NS(RigidBodyDynamicsType)

}   // namespace rigidbodyaspect

Q_DECLARE_METATYPE(rigidbodyaspect::RigidBodyDynamicsType)

#endif   // RIGIDBODYASPECT_TYPES_H
