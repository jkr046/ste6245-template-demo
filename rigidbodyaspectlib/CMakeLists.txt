# Minimum CMake version required
cmake_minimum_required(VERSION 3.8)

# Global name of the project
project(rigidbodyaspectlib VERSION 0.1 LANGUAGES CXX)


# Source files and friends
set( SRCS_PREFIX sources/rigidbodyaspect)
set( HDRS
  ${SRCS_PREFIX}/init.h
  ${SRCS_PREFIX}/constants.h
  ${SRCS_PREFIX}/types.h
  ${SRCS_PREFIX}/utils.h
  ${SRCS_PREFIX}/jobs/genericcolliderjob.h
  ${SRCS_PREFIX}/jobs/genericsimjob.h
  ${SRCS_PREFIX}/jobs/genericupdatejob.h
  ${SRCS_PREFIX}/frontend/abstractrigidbodycontroller.h
  ${SRCS_PREFIX}/frontend/planecontroller.h
  ${SRCS_PREFIX}/frontend/spherecontroller.h
  ${SRCS_PREFIX}/frontend/simulatorsettings.h
  ${SRCS_PREFIX}/frontend/environment.h
  ${SRCS_PREFIX}/backend/rigidbodyaspect.h
  ${SRCS_PREFIX}/backend/planecontrollerbackend.h
  ${SRCS_PREFIX}/backend/spherecontrollerbackend.h
  ${SRCS_PREFIX}/backend/simulatorsettingsbackend.h
  ${SRCS_PREFIX}/backend/environmentbackend.h
  ${SRCS_PREFIX}/geometry/movingbody.h
  ${SRCS_PREFIX}/geometry/fixedbody.h
  ${SRCS_PREFIX}/geometry/rbtypes.h
  ${SRCS_PREFIX}/geometry/rigidbodycontainer.h
  ${SRCS_PREFIX}/algorithms/simulation.h
  ${SRCS_PREFIX}/algorithms/collision.h
  )

set( SRCS
  ${SRCS_PREFIX}/utils.cpp
  ${SRCS_PREFIX}/jobs/genericcolliderjob.cpp
  ${SRCS_PREFIX}/jobs/genericsimjob.cpp
  ${SRCS_PREFIX}/jobs/genericupdatejob.cpp
  ${SRCS_PREFIX}/frontend/abstractrigidbodycontroller.cpp
  ${SRCS_PREFIX}/frontend/planecontroller.cpp
  ${SRCS_PREFIX}/frontend/spherecontroller.cpp
  ${SRCS_PREFIX}/frontend/simulatorsettings.cpp
  ${SRCS_PREFIX}/frontend/environment.cpp
  ${SRCS_PREFIX}/backend/rigidbodyaspect.cpp
  ${SRCS_PREFIX}/backend/planecontrollerbackend.cpp
  ${SRCS_PREFIX}/backend/spherecontrollerbackend.cpp
  ${SRCS_PREFIX}/backend/simulatorsettingsbackend.cpp
  ${SRCS_PREFIX}/backend/environmentbackend.cpp
  ${SRCS_PREFIX}/geometry/rigidbodycontainer.cpp
  ${SRCS_PREFIX}/algorithms/simulation.cpp
  ${SRCS_PREFIX}/algorithms/collision.cpp
  )

qt5_wrap_cpp( HDRS_MOC
  ${SRCS_PREFIX}/types.h
  ${SRCS_PREFIX}/frontend/abstractrigidbodycontroller.h
  ${SRCS_PREFIX}/frontend/planecontroller.h
  ${SRCS_PREFIX}/frontend/spherecontroller.h
  ${SRCS_PREFIX}/frontend/simulatorsettings.h
  ${SRCS_PREFIX}/frontend/environment.h
  ${SRCS_PREFIX}/backend/rigidbodyaspect.h
  )






#########################
# Define target: libarary
add_library( ${PROJECT_NAME} STATIC
  ${HDRS} ${SRCS} ${HDRS_MOC} )


##################
# Add header paths
target_include_directories( ${PROJECT_NAME} PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/sources> )



##################
# Compiler options

# Features
target_compile_features(${PROJECT_NAME}
  PUBLIC $<$<CXX_COMPILER_ID:Clang>:cxx_std_17>
  PUBLIC $<$<CXX_COMPILER_ID:GNU>:cxx_std_17>
  PUBLIC $<$<CXX_COMPILER_ID:MSVC>:cxx_std_17>
  )

# Definitions
target_compile_definitions( ${PROJECT_NAME}
  PUBLIC $<$<CXX_COMPILER_ID:MSVC>:
    _USE_MATH_DEFINES
    >
  )

# Compiler spesific options
target_compile_options(${PROJECT_NAME}
  PUBLIC $<$<CXX_COMPILER_ID:Clang>:
    ${MY_COMPILE_FLAGS_CLANG}
#    -Weverything -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-documentation -pedantic -Werror
#    -Wno-redundant-parens # Qt
#    -Wno-float-equal #Qt - member property binding
#    -Wno-language-extension-token
#    -Wno-unused-member-function  # Anoying during debug
    >
  PUBLIC $<$<CXX_COMPILER_ID:GNU>:${MY_COMPILE_FLAGS_GNU}>
  PUBLIC $<$<CXX_COMPILER_ID:MSVC>:${MY_COMPILE_FLAGS_MSVC}>
    )


###################
# Configure linking
target_link_libraries( ${PROJECT_NAME} PUBLIC
  ${QT_TARGET_KEYWORDS}
  gmlib2::gmlib2qt )
