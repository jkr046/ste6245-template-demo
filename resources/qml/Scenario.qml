import QtQuick 2.0
import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "qrc:///ex_qml/shared_assets/cameras"
import "qrc:///ex_qml/shared_assets/framegraphs"
import "qrc:///ex_qml/shared_assets/environments"

Item {
  id: single_camera_scene

  anchors.leftMargin: 2
  anchors.topMargin: 2
  anchors.rightMargin: 2
  anchors.bottomMargin: 2

  default property alias content: scene_root.childNodes
  property string scene_element_name: "scene_root_entity"
  property alias scenemodel: scene_root.scenemodel
  property alias simulatorRunStatus : simulator_settings.runStatus

  Scene3D {
    id: scene3d
    objectName: "scene3d"
    anchors.fill: parent
    focus: true
    aspects: ["input","logic","animation","rigidbodyaspect"]
    cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

    Entity {
      id: root_entity

      components: [ input_settings, render_settings]

      RenderSettings {
        id: render_settings
        activeFrameGraph: single_viewport_framegraph
        pickingSettings.pickMethod: PickingSettings.TrianglePicking
      }

      InputSettings { id: input_settings }

      RB.SimulatorSettings { id: simulator_settings; runStatus: false }


      SingleViewportFrameGraph {
        id: single_viewport_framegraph
        camera: main_camera
      }

      ProjectionCamera {
        id: main_camera
        position: Qt.vector3d(0.0, 20.0, 20.0)
        viewCenter: Qt.vector3d(0.0, 0.0, 0.0)
        upVector: Qt.vector3d(0.0, 1.0, 0.0)
      }

      OrbitCameraController{ camera: main_camera }

      SceneRootEntity {
        id: scene_root
        objectName: scene_element_name
      }

      DefaultSkyboxLightEnvironment {}

    } // END Entity (id:scene_root)

  } // END Scene3D


}
