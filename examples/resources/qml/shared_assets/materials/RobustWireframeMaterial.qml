import QtQuick 2.0

import Qt3D.Render 2.0
//import Qt3D.Extras 2.0

Material {

  property color ambient: Qt.rgba( 0.5, 0.0, 0.0, 1.0 )
  property color diffuse: Qt.rgba( 0.8, 0.0, 0.0, 1.0 )
  property color specular: Qt.rgba( 0.95, 0.95, 0.95, 1.0 )
  property real shininess: 150.0
  property real lineWidth: 0.8
  property color lineColor: Qt.rgba( 0.0, 0.0, 0.0, 1.0 )

  parameters: [
    Parameter { name: "ka"; value: Qt.vector3d(wireframe_material.ambient.r, wireframe_material.ambient.g, wireframe_material.ambient.b) },
    Parameter { name: "kd"; value: Qt.vector3d(wireframe_material.diffuse.r, wireframe_material.diffuse.g, wireframe_material.diffuse.b) },
    Parameter { name: "ksp"; value: Qt.vector3d(wireframe_material.specular.r, wireframe_material.specular.g, wireframe_material.specular.b) },
    Parameter { name: "shininess"; value: wireframe_material.shininess },
    Parameter { name: "line.width"; value: wireframe_material.lineWidth },
    Parameter { name: "line.color"; value: wireframe_material.lineColor }
  ]

  effect: Effect {
    parameters: [
      Parameter { name: "ka";   value: Qt.vector3d( 0.1, 0.1, 0.1 ) },
      Parameter { name: "kd";   value: Qt.vector3d( 0.7, 0.7, 0.7 ) },
      Parameter { name: "ks";  value: Qt.vector3d( 0.95, 0.95, 0.95 ) },
      Parameter { name: "shininess"; value: 150.0 }
    ]

    techniques: [
      Technique {
        graphicsApiFilter {
          api: GraphicsApiFilter.OpenGL
          profile: GraphicsApiFilter.CoreProfile
          majorVersion: 3
          minorVersion: 1
        }

        filterKeys: [ FilterKey { name: "renderingStyle"; value: "forward" } ]

        parameters: [
          Parameter { name: "light.position"; value: Qt.vector4d( 0.0, 0.0, 0.0, 1.0 ) },
          Parameter { name: "light.intensity"; value: Qt.vector3d( 1.0, 1.0, 1.0 ) },
          Parameter { name: "line.width"; value: 1.0 },
          Parameter { name: "line.color"; value: Qt.vector4d( 1.0, 1.0, 1.0, 1.0 ) }
        ]

        renderPasses: [
          RenderPass {
            shaderProgram: ShaderProgram {
              vertexShaderCode:   loadSource("qrc:///ex_shaders/robustwireframe/robustwireframe.vert")
              geometryShaderCode: loadSource("qrc:///ex_shaders/robustwireframe/robustwireframe.geom")
              fragmentShaderCode: loadSource("qrc:///ex_shaders/robustwireframe/robustwireframe.frag")
            }
          }
        ]
      }
    ]
  }
}
