import QtQuick 2.0
import QtQuick.Layouts 1.3

ComponentDelegate {
  title: "Transform"

  Text{text:"Matrix"}
  GridLayout{
    anchors.leftMargin: 5
    anchors.left: parent.left
    anchors.right: parent.right

    columns: 4

    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m11 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m12 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m13 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m14 }}

    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m21 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m22 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m23 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m24 }}

    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m31 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m32 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m33 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m34 }}

    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m41 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m42 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m43 }}
    Item{width:parent.width/4;height:width; Text{anchors.centerIn: parent; text: component_object.matrix.m44 }}
  }
}
