import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
  anchors.topMargin: 10
  anchors.leftMargin: 5
  anchors.rightMargin: 5
  width: parent.width


  default property alias _contenChildren: content.data
  property int componentHeight: content.y + content.height
  property alias title: header_text.text

  property bool expanded: false

  height: componentHeight



  Component.onCompleted: {
      componentHeight = Qt.binding(function(){
        if(expanded)
          return content.y + content.height
        else
          return content.y
      })
  }


  Rectangle {
    id: header
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right

    height: column_layout.height

    Column {
      id: column_layout
      anchors.top: parent.top
      anchors.left: parent.left
      anchors.right: parent.right
      height: row_layout.height + horizontal_line.height

      RowLayout{ id: row_layout;
        anchors.left: parent.left
        anchors.right: parent.right
        height: header_text.height;

        Text{ id: header_text }
        Item{Layout.fillWidth: true}
        Item{
          height: expand_text.height
          width: height

          Text{
            id:expand_text; anchors.centerIn: parent;
            text: expanded?"-":"+";

            MouseArea{ anchors.fill: parent; onClicked: expanded = !expanded }
          }
        }
      }
      Rectangle{ id: horizontal_line;
        height: 1; width: parent.width;
        color: "gray"
      }
    }
  }

  Column {
    id: content
    anchors.top: header.bottom
    anchors.left: parent.left
    anchors.right: parent.right

    visible: expanded
  }
}
