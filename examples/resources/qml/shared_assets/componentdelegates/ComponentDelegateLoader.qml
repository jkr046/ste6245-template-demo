import QtQuick 2.0

import com.uit.GMlib2Qt 1.0

Loader {

  width: parent.width

  function componentTypeToDelegateSource(component_type) {

    if(component_type === SceneObjectComponentModel.Material)
      return "MaterialComponentDelegate.qml"
    else if(component_type === SceneObjectComponentModel.Mesh)
      return "MeshComponentDelegate.qml"
    else if(component_type === SceneObjectComponentModel.ObjectPicker)
      return "ObjectPickerComponentDelegate.qml"
    else if(component_type === SceneObjectComponentModel.Transform)
      return "TransformComponentDelegate.qml"
    else return "UnknownComponentDelegate.qml"
  }

  source: componentTypeToDelegateSource(component_type)
}
