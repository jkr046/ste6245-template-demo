import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0

RenderSurfaceSelector {
  id: surface_selector

  property alias camera: fw_renderer.camera
  property alias clearColor: clear_buffers.clearColor

  ClearBuffers {
    id: clear_buffers

    buffers: ClearBuffers.ColorDepthBuffer
    clearColor: "#c0c0c0"

    ForwardRendererTechnique {
      id: fw_renderer
    }
  }

}
