import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0

Viewport {
  property alias camera: camera_selector.camera
  property alias cull_face_mode : cull_face.mode

  normalizedRect: Qt.rect(0.0, 0.0, 1.0, 1.0)

  TechniqueFilter {

    matchAll: [ FilterKey {name: "renderingStyle"; value: "forward"} ]

    RenderStateSet {
      renderStates: [
        DepthTest{ depthFunction: DepthTest.Less },
        CullFace{ id: cull_face; mode:CullFace.NoCulling}
      ]

      CameraSelector {
        id: camera_selector
      }
    }
  }
}
