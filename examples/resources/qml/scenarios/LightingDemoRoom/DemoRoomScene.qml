import Qt3D.Core 2.0
import Qt3D.Render 2.9
import Qt3D.Input 2.0
import Qt3D.Extras 2.9
import QtQuick 2.0

  Entity {
    id: orientation_cube

    // Materials
    GoochMaterial{ id: cube_side_gooch_material;
//        cool: "yellow"
//        warm: "black"
    }
    DiffuseMapMaterial{ id: cube_side_dmap_material;
        ambient: Qt.rgba( 0.05, 0.05, 0.05, 1.0 )
        diffuse: TextureLoader{ source: "qrc:///ex_textures/checker_pattern/checker_pattern_8.png"}
        specular: Qt.rgba( 1.00, 1.00, 1.00, 1.0 )
        shininess: 0.5
        textureScale: 0.1
    }
//    PhongAlphaMaterial{ id: cube_side_material; diffuse: "#d8d8d8"; alpha: 0.55}
//    PhongMaterial{ id: cube_frame_material; diffuse: "#c8c8c8"; ambient: diffuse}
//    PhongMaterial{ id: ori_base_material; diffuse: "#FFFFFF"; ambient: diffuse}
//    PhongMaterial{ id: ori_X_material; diffuse: "#FF0000"; ambient: diffuse}
//    PhongMaterial{ id: ori_Y_material; diffuse: "#00FF00"; ambient: diffuse}
//    PhongMaterial{ id: ori_Z_material; diffuse: "#0000FF"; ambient: diffuse}


    // Meshes
//    SphereMesh { id: ori_sphere_mesh; radius: 0.05 }
//    CylinderMesh{ id: ori_cyl_mesh; length:0.8; radius: 0.05}
//    ConeMesh { id: ori_cone_mesh; bottomRadius: 0.05; length: 0.15 }
    PlaneMesh { id: cube_side_mesh; height: 30; width: height }
    SphereMesh { id: light_sphere_mesh; radius: 1.8 }
//    CylinderMesh{id: cube_frame_cylinder_mesh; length:1.0; radius: 0.01}

    // Orientation entities
//    Entity { // Origin sphere
//      Transform{ id: ori_sph_trans; translation: Qt.vector3d(-0.45,-0.45,-0.45)}
//      components: [ori_sphere_mesh,ori_base_material,ori_sph_trans]
//    }

//    Entity { // X-cylinder
//      Transform{ id: ori_X_cyl_trans; translation: Qt.vector3d(-0.05,-0.45,-0.45); rotationZ: -90}
//      components: [ori_cyl_mesh,ori_X_material,ori_X_cyl_trans]
//    }

//    Entity { // X-cone
//      Transform{ id: ori_X_cone_trans; translation: Qt.vector3d(0.425,-0.45,-0.45); rotationZ: -90}
//      components: [ori_cone_mesh, ori_X_material, ori_X_cone_trans ]
//    }

//    Entity { // Y-cylinder
//      Transform{ id: ori_Y_cyl_trans; translation: Qt.vector3d(-0.45,-0.05,-0.45)}
//      components: [ori_cyl_mesh,ori_Y_material,ori_Y_cyl_trans]
//    }

//    Entity { // Y-cone
//      Transform{ id: ori_Y_cone_trans;translation: Qt.vector3d(-0.45,0.425,-0.45)}
//      components: [ori_cone_mesh,ori_Y_material,ori_Y_cone_trans]
//    }

//    Entity { // Z-cylinder
//      Transform{ id: ori_Z_cyl_trans; translation: Qt.vector3d(-0.45,-0.45,-0.05); rotationX: 90 }
//      components: [ori_cyl_mesh,ori_Z_material,ori_Z_cyl_trans]
//    }

//    Entity { // Z-cone
//      Transform{ id: ori_Z_cone_trans; translation: Qt.vector3d(-0.45,-0.45,0.425); rotationX: 90}
//      components: [ori_cone_mesh,ori_Z_material,ori_Z_cone_trans]
//    }





    Entity { // Top
      Transform{ id: top_transform; translation: Qt.vector3d(0.0,15.0,0.0)}
      components: [cube_side_mesh,cube_side_gooch_material,top_transform]
    }

    Entity { // Bottom
      Transform{ id: bottom_transform; translation: Qt.vector3d(0.0,-15.0,0.0); rotationX: 180}
      components: [cube_side_mesh,cube_side_gooch_material,bottom_transform]
    }

    Entity { // Left
      Transform{ id: left_transform; translation: Qt.vector3d(-15.0,0.0,0.0); rotationZ: 90 }
      components: [cube_side_mesh,cube_side_gooch_material,left_transform]
    }

    Entity { // Right
      Transform{ id: right_transform; translation: Qt.vector3d(15.0,0.0,0.0); rotationZ: -90}
      components: [cube_side_mesh,cube_side_gooch_material,right_transform]
    }

    Entity { // Front
      Transform{id: front_transform; translation: Qt.vector3d(0.0,0.0,15.0); rotationX: -90; rotationY: 180}
      components: [cube_side_mesh,cube_side_gooch_material,front_transform]
    }

    Entity { // Back
      TextureMaterial { id: bah; texture: TextureLoader{ source: "qrc:///ex_textures/qt/qt_logo.jpg" } }
      Transform{ id: back_transform; translation: Qt.vector3d(0.0,0.0,-15.0); rotationX: 90; rotationY: 180 }
      components: [cube_side_mesh,/*cube_side_dmap_material,*/bah,back_transform]
    }


    Entity { // Top-back-left light ball
      PointLight{ id: top_back_left_light;
          color: "red"
          intensity: 1.0
          constantAttenuation: 1.0
          linearAttenuation: 0.0
          quadraticAttenuation: 0.0025      }
      PhongMaterial{ id: top_back_left_light_mat; diffuse: top_back_left_light.color; ambient: diffuse }
      Transform{ id: top_back_left_light_trans; translation: Qt.vector3d(-8.0,8.0,-8.0) }
      components:  [light_sphere_mesh,top_back_left_light_mat,top_back_left_light_trans]
    }

    Entity { // Top-back-right light ball
      PointLight{ id: top_back_right_light;
          color: "blue"
          intensity: 1.0
          constantAttenuation: 1.0
          linearAttenuation: 0.0
          quadraticAttenuation: 0.0025      }
      PhongMaterial{ id: top_back_right_light_mat; diffuse: top_back_right_light.color; ambient: diffuse }
      Transform{ id: top_back_right_light_trans; translation: Qt.vector3d(8.0,8.0,-8.0) }
      components:  [light_sphere_mesh,top_back_right_light_mat,top_back_right_light_trans]
    }



    // corner spheres
//    Entity { Transform{ id: cs_trans01; translation: Qt.vector3d(-0.5,-0.5,-0.5)}
//      components: [ cube_frame_corner_mesh, cube_frame_material, cs_trans01]
//    }
//    Entity { Transform{ id: cs_trans02; translation: Qt.vector3d(0.5,-0.5,-0.5)}
//      components: [ cube_frame_corner_mesh, cube_frame_material, cs_trans02]
//    }
//    Entity { Transform{ id: cs_trans03; translation: Qt.vector3d(0.5,-0.5,0.5)}
//      components: [ cube_frame_corner_mesh, cube_frame_material, cs_trans03]
//    }
//    Entity { Transform{ id: cs_trans04; translation: Qt.vector3d(-0.5,-0.5,0.5)}
//      components: [ cube_frame_corner_mesh, cube_frame_material, cs_trans04]
//    }
//    Entity { Transform{ id: cs_trans05; translation: Qt.vector3d(-0.5,0.5,-0.5)}
//      components: [ cube_frame_corner_mesh, cube_frame_material, cs_trans05]
//    }
//    Entity { Transform{ id: cs_trans06; translation: Qt.vector3d(0.5,0.5,-0.5)}
//      components: [ cube_frame_corner_mesh, cube_frame_material, cs_trans06]
//    }
//    Entity { Transform{ id: cs_trans07; translation: Qt.vector3d(0.5,0.5,0.5)}
//      components: [ cube_frame_corner_mesh, cube_frame_material, cs_trans07]
//    }
//    Entity { Transform{ id: cs_trans08; translation: Qt.vector3d(-0.5,0.5,0.5)}
//      components: [ cube_frame_corner_mesh, cube_frame_material, cs_trans08]
//    }

    // edge cylinders
//    Entity {  Transform{id: ec_trans01; translation: Qt.vector3d(-0.5,-0.5,0.0); rotationX: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans01]
//    }
//    Entity { Transform{id: ec_trans02; translation: Qt.vector3d(0.5,-0.5,0.0); rotationX: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans02]
//    }
//    Entity { Transform{id: ec_trans03; translation: Qt.vector3d(0.0,-0.5,-0.5); rotationZ: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans03]
//    }
//    Entity { Transform{id: ec_trans04; translation: Qt.vector3d(0.0,-0.5,0.5); rotationZ: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans04]
//    }
//    Entity { Transform{id: ec_trans05; translation: Qt.vector3d(-0.5,0.5,0.0); rotationX: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans05]
//    }
//    Entity { Transform{id: ec_trans06; translation: Qt.vector3d(0.5,0.5,0.0); rotationX: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans06]
//    }
//    Entity { Transform{id: ec_trans07; translation: Qt.vector3d(0.0,0.5,-0.5); rotationZ: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans07]
//    }
//    Entity { Transform{id: ec_trans08; translation: Qt.vector3d(0.0,0.5,0.5); rotationZ: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans08]
//    }
//    Entity { Transform{id: ec_trans09; translation: Qt.vector3d(-0.5,0.0,-0.5); rotationY: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans09]
//    }
//    Entity { Transform{id: ec_trans10; translation: Qt.vector3d(0.5,0.0,-0.5); rotationY: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans10]
//    }
//    Entity { Transform{id: ec_trans11; translation: Qt.vector3d(0.5,0.0,0.5); rotationY: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans11]
//    }
//    Entity { Transform{id: ec_trans12; translation: Qt.vector3d(-0.5,0.0,0.5); rotationY: 90 }
//      components: [cube_frame_cylinder_mesh,cube_frame_material,ec_trans12]
//    }
  }
