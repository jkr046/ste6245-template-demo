import QtQuick 2.0
import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.9
import Qt3D.Input 2.0
import Qt3D.Extras 2.9

RenderSurfaceSelector {
  id: root

  readonly property alias scene_layer : scene_layer
  readonly property alias overlay_layer : overlay_layer
  property var select_layer : 0

  Layer {
    id: scene_layer
    recursive: true
  }

  Layer {
    id: overlay_layer
    recursive: true
  }


  Viewport { normalizedRect: Qt.rect(0,0,1,1)

    ClearBuffers {
      buffers: ClearBuffers.ColorDepthBuffer
      clearColor: "#c0c0c0"
      NoDraw{}
    }

    TechniqueFilter {
      matchAll: [ FilterKey {name: "renderingStyle"; value: "forward"} ]

      // Scene
      RenderStateSet {
        renderStates: [
          DepthTest{ depthFunction: DepthTest.Less},
          CullFace{ mode:CullFace.NoCulling},
//          CullFace{ mode:CullFace.Front},
          BlendEquation{ blendFunction: BlendEquation.Add },
          BlendEquationArguments {
            sourceRgb: BlendEquationArguments.SourceAlpha
            destinationRgb: BlendEquationArguments.OneMinusSourceAlpha
            sourceAlpha: BlendEquationArguments.One
            destinationAlpha: BlendEquationArguments.Zero
          }
        ]

        LayerFilter {
          layers: [scene_layer]
          filterMode: LayerFilter.AcceptAnyMatchingLayers

          CameraSelector {
            camera: projection_camera
          }
        }
      }

      // Selected objects
      RenderStateSet {
        renderStates: [
          DepthTest{ depthFunction: DepthTest.LessOrEqual},
          CullFace{ mode:CullFace.NoCulling},
          BlendEquation{ blendFunction: BlendEquation.Add },
          BlendEquationArguments {
            sourceRgb: BlendEquationArguments.SourceAlpha
            destinationRgb: BlendEquationArguments.One
            sourceAlpha: BlendEquationArguments.One
            destinationAlpha: BlendEquationArguments.Zero
          }
        ]

        LayerFilter {
          layers: [select_layer]
          filterMode: LayerFilter.AcceptAnyMatchingLayers

          CameraSelector {
            camera: projection_camera
          }
        }
      }

      // Overlay
      ClearBuffers {
        buffers: ClearBuffers.DepthBuffer

        RenderStateSet {
          renderStates: [
            DepthTest{ depthFunction: DepthTest.Less},
            CullFace{ mode:CullFace.Back},
            BlendEquation{ blendFunction: BlendEquation.Add },
            BlendEquationArguments {
              sourceRgb: BlendEquationArguments.SourceAlpha
              destinationRgb: BlendEquationArguments.OneMinusSourceAlpha
              sourceAlpha: BlendEquationArguments.One
              destinationAlpha: BlendEquationArguments.Zero
            }
          ]

          LayerFilter { layers: [overlay_layer]
            filterMode: LayerFilter.AcceptAnyMatchingLayers
            CameraSelector { camera: overlay_scene.cubeCamera }
          }
        }
      }
    }
  }
}
