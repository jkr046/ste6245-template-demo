import Qt3D.Core 2.0
import Qt3D.Render 2.9
import Qt3D.Input 2.0
import Qt3D.Extras 2.9
import QtQuick 2.0

import "../../shared_assets/cameras"
import "../../shared_assets/3dobjects"

Entity {
  id: scene_overlay

  property alias cubeViewMatrix: cube_transform.cM
  readonly property alias cubeCamera : orientation_cube_camera

  Entity {
       components: [
           DirectionalLight {
               worldDirection: Qt.vector3d(0.3, -3.0, 0.0).normalized();
               color: "#fbf9ce"
               intensity: 0.3
           }
       ]
   }

   Entity {
       components: [
           DirectionalLight {
               worldDirection: Qt.vector3d(-0.3, -0.3, 0.0).normalized();
               color: "#9cdaef"
               intensity: 0.15
           }
       ]
   }

  ProjectionCamera {
    id: orientation_cube_camera

    position: Qt.vector3d(0.0, 0.0, 2.5)
    viewCenter: Qt.vector3d(0.0,0.0,0.0)
    upVector: Qt.vector3d(0.0, 1.0, 0.0)
  }

  Entity {

    // Platform
    Entity {
      MetalRoughMaterial{ id: cube_base_material; baseColor: "#fdd8dd" }
      MetalRoughMaterial{ id: cube_base_text_material; baseColor: "#FF0000" }
      CuboidMesh { id: cube_base_mesh; xExtent: 1.1; yExtent: 0.1; zExtent: 1.1 }
      Transform { id: cube_base_trans; translation: Qt.vector3d(0.0,-0.9,0.0) }

      components: [cube_base_mesh,cube_base_material,cube_base_trans]

      Entity {
        ExtrudedTextMesh {id: gm_text_mesh; depth: 0.5; text: "Arctic University of Norway"; font.family: "Helvetica"; font.pointSize: 34; font.bold: true}
        Transform{ id: gm_text_transform; scale3D: Qt.vector3d(0.052,0.06,0.06); translation: Qt.vector3d(-0.52,-0.02,0.55) }
        components: [gm_text_mesh,cube_base_text_material,gm_text_transform]
      }
    }

    GMlib2Cube {
      id: gmlib2cube
      components: [cube_transform]
      Transform {
        id: cube_transform
        property matrix4x4 cM
        property vector3d cU: Qt.vector3d(0,1,0)
        property vector3d cV: Qt.vector3d(1,0,0)

        Component.onCompleted: {
          matrix =  Qt.binding(function(){
            var mat = cM
            return Qt.matrix4x4(
                    mat.m11, mat.m12, mat.m13, 0,
                    mat.m21, mat.m22, mat.m23, 0,
                    mat.m31, mat.m32, mat.m33, 0,
                          0,       0,       0, 1)
          })
        }
      }
    }

    components: [ Transform{ scale: 0.2; translation: Qt.vector3d(-0.8,-0.75,0) } ]
  }

}
